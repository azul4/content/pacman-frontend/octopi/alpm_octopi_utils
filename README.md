# alpm_octopi_utils

Alpm utils for Octopi

https://tintaescura.com/projects/octopi/

https://github.com/aarnt/octopi


How to clone this repository:

```
git clone https://gitlab.com/azul4/content/pacman-frontend/octopi/alpm_octopi_utils.git
```
